import copy
from collections import ChainMap

import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
ACCOUNT_NAME = "my-storage-account"
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "sku_tier": "Standard",
    "sku_name": "Standard_LRS",
    "account_kind": "BlockBlobStorage",
    "is_hns_enabled": "true",
    "nfsv3_enabled": "true",
    "enable_https_traffic_only": "false",
    "min_tls_version": "TLS1_2",
    "allow_blob_public_access": "true",
    "allow_shared_key_access": "true",
}
RESOURCE_PARAMETERS_RAW = {
    "sku": {
        "name": "Standard_LRS",
        "tier": "Standard",
    },
    "kind": "BlockBlobStorage",
    "location": "eastus",
    "properties": {
        "isHnsEnabled": "true",
        "isNfsV3Enabled": "true",
        "supportsHttpsTrafficOnly": "false",
        "minimumTlsVersion": "TLS1_2",
        "allowBlobPublicAccess": "true",
        "allowSharedKeyAccess": "true",
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of storage account. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.present = (
        hub.states.azure.storage_resource_provider.storage_accounts.present
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_present_to_raw_storage_accounts = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_present_to_raw_storage_accounts
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.update_storage_accounts_payload = (
        hub.tool.azure.storage_resource_provider.storage_accounts.update_storage_accounts_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.storage_resource_provider.storage_accounts",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        assert json == RESOURCE_PARAMETERS_RAW
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.present(
            test_ctx,
            RESOURCE_NAME,
            RESOURCE_GROUP_NAME,
            ACCOUNT_NAME,
            **RESOURCE_PARAMETERS,
        )
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.present(
            ctx,
            RESOURCE_NAME,
            RESOURCE_GROUP_NAME,
            ACCOUNT_NAME,
            **RESOURCE_PARAMETERS,
        )
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of storage account. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.present = (
        hub.states.azure.storage_resource_provider.storage_accounts.present
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_present_to_raw_storage_accounts = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_present_to_raw_storage_accounts
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.update_storage_accounts_payload = (
        hub.tool.azure.storage_resource_provider.storage_accounts.update_storage_accounts_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "location": "eastus",
        "sku_tier": "Standard",
        "sku_name": "Standard_LRS",
        "account_kind": "BlockBlobStorage",
        "is_hns_enabled": "true",
        "nfsv3_enabled": "true",
        "enable_https_traffic_only": "false",
        "min_tls_version": "TLS1_1",
        "allow_blob_public_access": "true",
        "allow_shared_key_access": "true",
    }
    resource_parameters_update_raw = {
        "sku": {
            "name": "Standard_LRS",
            "tier": "Standard",
        },
        "kind": "BlockBlobStorage",
        "location": "eastus",
        "properties": {
            "isHnsEnabled": "true",
            "isNfsV3Enabled": "true",
            "supportsHttpsTrafficOnly": "false",
            "minimumTlsVersion": "TLS1_1",
            "allowBlobPublicAccess": "true",
            "allowSharedKeyAccess": "true",
        },
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        assert resource_parameters_update_raw["location"] == json.get("location")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.present(
            test_ctx,
            RESOURCE_NAME,
            RESOURCE_GROUP_NAME,
            ACCOUNT_NAME,
            **resource_parameters_update,
        )
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.present(
            ctx,
            RESOURCE_NAME,
            RESOURCE_GROUP_NAME,
            ACCOUNT_NAME,
            **resource_parameters_update,
        )
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of storage accounts. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.absent = (
        hub.states.azure.storage_resource_provider.storage_accounts.absent
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.storage_resource_provider.storage_accounts.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, ACCOUNT_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of storage account. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.absent = (
        hub.states.azure.storage_resource_provider.storage_accounts.absent
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.storage_resource_provider.storage_accounts.absent(
        test_ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, ACCOUNT_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.storage_resource_provider.storage_accounts.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, ACCOUNT_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.storage_resource_provider.storage_accounts '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of public-ip-address.
    """
    mock_hub.states.azure.storage_resource_provider.storage_accounts.describe = (
        hub.states.azure.storage_resource_provider.storage_accounts.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )

    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = (
        await mock_hub.states.azure.storage_resource_provider.storage_accounts.describe(
            ctx
        )
    )

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert (
        "azure.storage_resource_provider.storage_accounts.present" in ret_value.keys()
    )
    described_resource = ret_value.get(
        "azure.storage_resource_provider.storage_accounts.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert ACCOUNT_NAME == old_state.get("account_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["sku_tier"] == old_state.get("sku_tier")
        assert expected_old_state["sku_name"] == old_state.get("sku_name")
        assert expected_old_state["account_kind"] == old_state.get("account_kind")
        assert expected_old_state["enable_https_traffic_only"] == old_state.get(
            "enable_https_traffic_only"
        )
        assert expected_old_state["min_tls_version"] == old_state.get("min_tls_version")
        assert expected_old_state["allow_blob_public_access"] == old_state.get(
            "allow_blob_public_access"
        )
        assert expected_old_state["allow_shared_key_access"] == old_state.get(
            "allow_shared_key_access"
        )
        assert expected_old_state["is_hns_enabled"] == old_state.get("is_hns_enabled")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert ACCOUNT_NAME == new_state.get("account_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["sku_tier"] == new_state.get("sku_tier")
        assert expected_new_state["sku_name"] == new_state.get("sku_name")
        assert expected_new_state["account_kind"] == new_state.get("account_kind")
        assert expected_new_state["enable_https_traffic_only"] == new_state.get(
            "enable_https_traffic_only"
        )
        assert expected_new_state["min_tls_version"] == new_state.get("min_tls_version")
        assert expected_new_state["allow_blob_public_access"] == new_state.get(
            "allow_blob_public_access"
        )
        assert expected_new_state["allow_shared_key_access"] == new_state.get(
            "allow_shared_key_access"
        )
        assert expected_new_state["is_hns_enabled"] == new_state.get("is_hns_enabled")
